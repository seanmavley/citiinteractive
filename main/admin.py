from django.contrib import admin
from .models import Person, Section

class PersonAdmin(admin.ModelAdmin):
  pass


class SectionAdmin(admin.ModelAdmin):
  pass

# Register your models here.
admin.site.register(Person, PersonAdmin)
admin.site.register(Section, SectionAdmin)