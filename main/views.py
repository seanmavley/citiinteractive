from django.shortcuts import render
from django.views.generic.list import ListView

from .models import Section
# Create your views here.

class SectionListView(ListView):
  model = Section
  template_name = 'index.html'
  # queryset = Section.objects.all().order_by('-createdAt')