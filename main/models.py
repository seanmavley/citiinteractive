from django.db import models
from .helper.helper_model import DateTimeStamped


class Section(DateTimeStamped):
  title = models.CharField(max_length=200)
  description = models.TextField();

  def __str__(self):
    return self.title

class Person(DateTimeStamped):
  photo = models.ImageField(upload_to='uploads')
  name = models.CharField(max_length=200)
  position = models.CharField(max_length=200)
  description = models.TextField(max_length=200)
  belongs_to = models.ForeignKey('Section', on_delete=models.CASCADE)

  def __str__(self):
    return self.name