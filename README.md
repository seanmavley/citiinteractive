## What is this repository for? ##

Something similar to this, for CitiFM Ghana

interactive.aljazeera.com/aje/2016/trump-cabinet-appointees/index.html

## Download Source HTML/CSS Files

Use this download link: 
https://bitbucket.org/seanmavley/citiinteractive/raw/6f83ff9c874cb2daefa612824d6b325a57580d53/CitiInteractive.zip

### How do I get set up? ###

Follow the tutorial here of how to deploy Django Projects, of which this is one of.

https://blog.khophi.co/postgresql-django-nginx-gunicorn-virtualenvwrapper-16-04-lts-ubuntu-server/

Note: It ain't WordPress, rather, Django!